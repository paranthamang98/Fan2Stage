import React from 'react'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import Logoin from '../Pages/authenticationScreens/logoin'
import Register from '../Pages/authenticationScreens/register'

function Navigate() {
  return (
    <>
    <BrowserRouter>
    <Routes>
        <Route path={"logoin"} element={<Logoin/>}/>
        <Route path={"register"} element={<Register/>}/>
    </Routes>
    </BrowserRouter>
      
    </>
  )
}

export default Navigate
