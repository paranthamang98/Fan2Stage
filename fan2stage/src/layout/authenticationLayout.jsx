import React from 'react'
import "./authenticationLayout.scss"
function AuthenticationLayout(props) {
  const{headeTitle ,subHeadTitle} = props
  
  return (
    <div className="authenticationLayout">
        <div className="authenticationLayout_bg"></div>
        <div className="authenticationLayout_input">
          <div className="authenticationLayout_input_innerSection">
            <h1>{headeTitle}</h1>
            <p>{subHeadTitle}</p>
            {props.children}
          </div>
        </div>
      
    </div>
  )
}

export default AuthenticationLayout
