import React, { useState } from 'react'
import { HideEyeIconS,OPenEyeIconS } from '../utils/images'
import "./component.scss"




const Fields = props => {
    const {label,placeholder,value,functions,name,type,icon}= props 

    const[eyeIcon,setEyeIcon]=useState(false)
  return (
    <div className='form_section_input'>
        <label htmlFor="">{label}</label>
        <input type={type === "password"? (eyeIcon) ? "text":"password":type} placeholder={placeholder} value={value} onChange={functions} name={name} />
        <span><img src={icon} alt="" /></span>
      {(type === "password") &&        
       <span className='eyeIcon' onClick={()=>{setEyeIcon(!eyeIcon)
     }}><img src={eyeIcon? OPenEyeIconS :HideEyeIconS } alt="icon" /></span>

}
    </div>
  )
}


export default Fields
