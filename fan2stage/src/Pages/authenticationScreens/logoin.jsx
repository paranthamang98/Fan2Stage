import React from 'react'
import { useState } from 'react';
import Fields from '../../component/fields';
import AuthenticationLayout from '../../layout/authenticationLayout';
import { EmailBoxIconS, PasswordIconS } from '../../utils/images';


function Logoin() {
  const [value, setValue] = useState(
    {
      email: "Jarvis19@marvel.com",
      password: "",
    }
  )
  const functions = (e) => {
    setValue({ [e.target.name]: e.target.value })
  }
  return (

    <AuthenticationLayout 
    headeTitle={"Welcome Back"} 
    subHeadTitle={"Login to continue using your account"}>

      <Fields
        label={"Email Address*"}
        value={value.email}
        placeholder={"Enter Email"}
        name={value.email}
        functions={functions}
        type={"email"} 
        icon={EmailBoxIconS}
        />

      <Fields
        label={"Enter Password"}
        value={value.password}
        placeholder={"Enter Password"}
        name={value.password}
        functions={functions}
        type={"password"}
        icon={PasswordIconS}
         />
         <p className='forgotSection'>Forgot Password?</p>
         <button className='authentication_button'>Login Now</button>

         <p className='opction_or'>OR</p>
         <p className='other_loging'>Login Using</p>
         <div className=" social_media_icon">
          <span></span>
          <span></span>
          <span></span>
          <span></span>
         </div>
         <p className='linkto_other'>Don’t Have an Account? Register Here</p>

    </AuthenticationLayout >


  )
}

export default Logoin
