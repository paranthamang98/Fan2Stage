import React from "react";
import { useState } from "react";
import Fields from "../../component/fields";
import AuthenticationLayout from "../../layout/authenticationLayout";
import { EmailBoxIconS, NameIcon, PasswordIconS, PhoneCallS } from '../../utils/images';

function Register() {
  const [value, setValue] = useState({
    Name: "",
    mobileNumber: "",
    email: "Jarvis19@marvel.com|",
    password: "",
    confirmPassword: "",
  });
  const functions = (e) => {
    setValue({ [e.target.name]: e.target.value })
  }
  return (
    <AuthenticationLayout 
    headeTitle={"Lets’s Get Started!"} 
    subHeadTitle={"Create an Account to Fan2Stage "}>
      <Fields
        label={"Full Name*"}
        value={value.email}
        placeholder={"Enter Email"}
        name={value.Name}
        functions={functions}
        type={"text"}
        icon={NameIcon}
      />

      <Fields
        label={"Mobile Number*"}
        value={value.password}
        placeholder={"Enter Password"}
        name={value.mobileNumber}
        functions={functions}
        type={"text"}
        icon={PhoneCallS}
      />

      <Fields
        label={"Email Address*"}
        value={value.password}
        placeholder={"Enter Password"}
        name={value.email}
        functions={functions}
        type={"email"}
        icon={EmailBoxIconS}
      />
      <Fields
        label={"Password*"}
        value={value.password}
        placeholder={"Enter Password"}
        name={value.password}
        functions={functions}
        type={"password"}
        icon={PasswordIconS}
      />
      <Fields
        label={"Confirm Password*"}
        value={value.password}
        placeholder={"Enter Password"}
        name={value.confirmPassword}
        functions={functions}
        type={"password"}
        icon={PasswordIconS}
      />
    </AuthenticationLayout>
  );
}

export default Register;
