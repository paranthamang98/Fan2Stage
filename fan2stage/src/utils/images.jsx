import  AuthenticationBg from "../asset/image/authentication_bg.png";
import  AuthLogo from "../asset/image/auth_Logo.png";
import  Header_logo from "../asset/image/header_logo.png";
import  ArrowDownSignS from "../asset/image/arrowDownSign.svg";
import  ArrowLeftS from "../asset/image/arrowLeft.svg";
import  CalendarNS from "../asset/image/calendarN.svg";
import  CheckBoxS from "../asset/image/checkBox.svg";
import  CheckRiadioS from "../asset/image/checkRiadio.svg";
import  ClockS from "../asset/image/clock.svg";
import  EmailBoxIconS from "../asset/image/emailBoxIcon.svg";
import  HideEyeIconS from "../asset/image/hideEyeIcon.svg";
import  OPenEyeIconS from "../asset/image/eye-open.svg";
import  NotificationsProfileS from "../asset/image/notificationsProfile.png";
import  PasswordIconS from "../asset/image/password_icon.svg";
import  PhoneCallS from "../asset/image/phoneCall.svg";
import  RadioS from "../asset/image/radio.svg";
import  SearchIconS from "../asset/image/searchIcon.svg";
import  SocialIconsGroup from "../asset/image/socialIcons.svg";
import  StareIconGrayS from "../asset/image/stareIconGray.svg";
import  UnCheckBoxS from "../asset/image/unCheckBox.svg";
import  CloseIconS from "../asset/image/X.svg";
import  StareIconBlackS from "../asset/image/stareIconBlack.svg";
import  NewsletterIcon from "../asset/image/verticalCard-rocket-icon.svg";
import  NameIcon from "../asset/image/name_input_icon.svg";
// import  NewsletterIcon from "../asset/image/phoneCall.svg";

export {
    AuthenticationBg,
    AuthLogo,
    ArrowDownSignS,
    ArrowLeftS,
    CalendarNS,
    CheckBoxS,
    CheckRiadioS,
    ClockS,
    EmailBoxIconS,
    HideEyeIconS,
    OPenEyeIconS,
    PasswordIconS,
    PhoneCallS,
    RadioS,
    SearchIconS,
    SocialIconsGroup,
    StareIconGrayS,
    UnCheckBoxS,
    CloseIconS,
    StareIconBlackS,
    NewsletterIcon,
    NotificationsProfileS,
    Header_logo,
    NameIcon





};